<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.7.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="11" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="11" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="DubSat1 Components">
<description>Shared Library (on github) for components used in Dubsat1</description>
<packages>
<package name="PC104_CONNECTOR">
<pad name="P$1" x="-31.75" y="32.134" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$2" x="-31.75" y="34.674" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$3" x="-31.75" y="37.214" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$4" x="-31.75" y="39.754" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$5" x="-29.21" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$6" x="-29.21" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$7" x="-29.21" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$8" x="-29.21" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$9" x="-26.67" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$10" x="-26.67" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$11" x="-26.67" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$12" x="-26.67" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$13" x="-24.13" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$14" x="-24.13" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$15" x="-24.13" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$16" x="-24.13" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$17" x="-21.59" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$18" x="-21.59" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$19" x="-21.59" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$20" x="-21.59" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$21" x="-19.05" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$22" x="-19.05" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$23" x="-19.05" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$24" x="-19.05" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$25" x="-16.51" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$26" x="-16.51" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$27" x="-16.51" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$28" x="-16.51" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$29" x="-13.97" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$30" x="-13.97" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$31" x="-13.97" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$32" x="-13.97" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$33" x="-11.43" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$34" x="-11.43" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$35" x="-11.43" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$36" x="-11.43" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$37" x="-8.89" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$38" x="-8.89" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$39" x="-8.89" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$40" x="-8.89" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$41" x="-6.35" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$42" x="-6.35" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$43" x="-6.35" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$44" x="-6.35" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$45" x="-3.81" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$46" x="-3.81" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$47" x="-3.81" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$48" x="-3.81" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$49" x="-1.27" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$50" x="-1.27" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$51" x="-1.27" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$52" x="-1.27" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$53" x="1.27" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$54" x="1.27" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$55" x="1.27" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$56" x="1.27" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$57" x="3.81" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$58" x="3.81" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$59" x="3.81" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$60" x="3.81" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$61" x="6.35" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$62" x="6.35" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$63" x="6.35" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$64" x="6.35" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$65" x="8.89" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$66" x="8.89" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$67" x="8.89" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$68" x="8.89" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$69" x="11.43" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$70" x="11.43" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$71" x="11.43" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$72" x="11.43" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$73" x="13.97" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$74" x="13.97" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$75" x="13.97" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$76" x="13.97" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$77" x="16.51" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$78" x="16.51" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$79" x="16.51" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$80" x="16.51" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$81" x="19.05" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$82" x="19.05" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$83" x="19.05" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$84" x="19.05" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$85" x="21.59" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$86" x="21.59" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$87" x="21.59" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$88" x="21.59" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$89" x="24.13" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$90" x="24.13" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$91" x="24.13" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$92" x="24.13" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$93" x="26.67" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$94" x="26.67" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$95" x="26.67" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$96" x="26.67" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$97" x="29.21" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$98" x="29.21" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$99" x="29.21" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$100" x="29.21" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$101" x="31.75" y="32.134" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$102" x="31.75" y="34.674" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$103" x="31.75" y="37.214" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$104" x="31.75" y="39.754" drill="0.9525" diameter="1.8796" shape="octagon"/>
<wire x1="-33.02" y1="30.61" x2="-33.02" y2="40.77" width="0.2032" layer="21"/>
<wire x1="-33.02" y1="40.77" x2="33.02" y2="40.77" width="0.2032" layer="21"/>
<wire x1="33.02" y1="40.77" x2="33.02" y2="30.61" width="0.2032" layer="21"/>
<wire x1="33.02" y1="30.61" x2="-33.02" y2="30.61" width="0.2032" layer="21"/>
<wire x1="0" y1="38.23" x2="0" y2="33.15" width="0" layer="21"/>
</package>
<package name="BOARD_TEMPLATE_SECURED">
<wire x1="-45" y1="-32.5" x2="-45" y2="32.5" width="0" layer="20"/>
<pad name="P$1" x="-39.5" y="39.5" drill="2.9" diameter="7.5"/>
<pad name="P$2" x="-39.5" y="-39.5" drill="2.9" diameter="7.5"/>
<pad name="P$3" x="39.5" y="-39.5" drill="2.9" diameter="7.5"/>
<pad name="P$4" x="39.5" y="39.5" drill="2.9" diameter="7.5"/>
<wire x1="-32.5" y1="45" x2="32.5" y2="45" width="0" layer="20"/>
<wire x1="45" y1="32.5" x2="45" y2="-32.5" width="0" layer="20"/>
<wire x1="32.5" y1="-45" x2="-32.5" y2="-45" width="0" layer="20"/>
<wire x1="-33.02" y1="30.61" x2="-33.02" y2="40.77" width="0" layer="21"/>
<wire x1="-33.02" y1="40.77" x2="33.02" y2="40.77" width="0" layer="21"/>
<wire x1="33.02" y1="40.77" x2="33.02" y2="30.61" width="0" layer="21"/>
<wire x1="33.02" y1="30.61" x2="-33.02" y2="30.61" width="0" layer="21"/>
<wire x1="-47" y1="47" x2="47" y2="47" width="0.127" layer="51"/>
<wire x1="47" y1="47" x2="47" y2="-47" width="0.127" layer="51"/>
<wire x1="47" y1="-47" x2="-47" y2="-47" width="0.127" layer="51"/>
<wire x1="-47" y1="-47" x2="-47" y2="47" width="0.127" layer="51"/>
<wire x1="-69" y1="34.5" x2="-34.5" y2="34.5" width="0.127" layer="51"/>
<wire x1="-34.5" y1="34.5" x2="-34.5" y2="69" width="0.127" layer="51"/>
<wire x1="34.5" y1="69" x2="34.5" y2="34.5" width="0.127" layer="51"/>
<wire x1="34.5" y1="34.5" x2="69" y2="34.5" width="0.127" layer="51"/>
<wire x1="34.5" y1="-69" x2="34.5" y2="-34.5" width="0.127" layer="51"/>
<wire x1="34.5" y1="-34.5" x2="69" y2="-34.5" width="0.127" layer="51"/>
<wire x1="-34.5" y1="-69" x2="-34.5" y2="-34.5" width="0.127" layer="51"/>
<wire x1="-34.5" y1="-34.5" x2="-69" y2="-34.5" width="0.127" layer="51"/>
<wire x1="-47" y1="-46" x2="-47" y2="-34.5" width="0" layer="20"/>
<wire x1="-47" y1="-34.5" x2="-45" y2="-32.5" width="0" layer="20" curve="90"/>
<wire x1="-46" y1="-47" x2="-34.5" y2="-47" width="0" layer="20"/>
<wire x1="-34.5" y1="-47" x2="-32.5" y2="-45" width="0" layer="20" curve="-90"/>
<wire x1="34.5" y1="-47" x2="46" y2="-47" width="0" layer="20"/>
<wire x1="47" y1="-46" x2="47" y2="-34.5" width="0" layer="20"/>
<wire x1="47" y1="-34.5" x2="45" y2="-32.5" width="0" layer="20" curve="-90"/>
<wire x1="34.5" y1="-47" x2="32.5" y2="-45" width="0" layer="20" curve="90"/>
<wire x1="34.5" y1="47" x2="46" y2="47" width="0" layer="20"/>
<wire x1="47" y1="46" x2="47" y2="34.5" width="0" layer="20"/>
<wire x1="47" y1="34.5" x2="45" y2="32.5" width="0" layer="20" curve="90"/>
<wire x1="34.5" y1="47" x2="32.5" y2="45" width="0" layer="20" curve="-90"/>
<wire x1="-47" y1="34.5" x2="-47" y2="46" width="0" layer="20"/>
<wire x1="-46" y1="47" x2="-34.5" y2="47" width="0" layer="20"/>
<wire x1="-34.5" y1="47" x2="-32.5" y2="45" width="0" layer="20" curve="90"/>
<wire x1="-47" y1="34.5" x2="-45" y2="32.5" width="0" layer="20" curve="-90"/>
<wire x1="-47" y1="46" x2="-46" y2="47" width="0" layer="20"/>
<wire x1="46" y1="47" x2="47" y2="46" width="0" layer="20"/>
<wire x1="46" y1="-47" x2="47" y2="-46" width="0" layer="20"/>
<wire x1="-47" y1="-46" x2="-46" y2="-47" width="0" layer="20"/>
</package>
<package name="BOARD_TEMPLATE" urn="urn:adsk.eagle:footprint:2229800/1">
<wire x1="-40" y1="-45" x2="-45" y2="-40" width="0" layer="20"/>
<wire x1="-45" y1="-40" x2="-45" y2="40" width="0" layer="20"/>
<wire x1="-45" y1="40" x2="-40" y2="45" width="0" layer="20"/>
<wire x1="-40" y1="45" x2="40" y2="45" width="0" layer="20"/>
<wire x1="40" y1="45" x2="45" y2="40" width="0" layer="20"/>
<wire x1="45" y1="40" x2="45" y2="-40" width="0" layer="20"/>
<wire x1="45" y1="-40" x2="40" y2="-45" width="0" layer="20"/>
<wire x1="40" y1="-45" x2="-40" y2="-45" width="0" layer="20"/>
<wire x1="-33.02" y1="30.61" x2="-33.02" y2="40.77" width="0" layer="21"/>
<wire x1="-33.02" y1="40.77" x2="33.02" y2="40.77" width="0" layer="21"/>
<wire x1="33.02" y1="40.77" x2="33.02" y2="30.61" width="0" layer="21"/>
<wire x1="33.02" y1="30.61" x2="-33.02" y2="30.61" width="0" layer="21"/>
<pad name="P$1" x="-39.5" y="39.5" drill="2.9" diameter="7.5"/>
<pad name="P$2" x="-39.5" y="-39.5" drill="2.9" diameter="7.5"/>
<pad name="P$3" x="39.5" y="-39.5" drill="2.9" diameter="7.5"/>
<pad name="P$4" x="39.5" y="39.5" drill="2.9" diameter="7.5"/>
</package>
<package name="BOARD_TEMPLATE_RAHS">
<wire x1="-40" y1="-45" x2="-45" y2="-40" width="0" layer="20"/>
<wire x1="-45" y1="-40" x2="-45" y2="40" width="0" layer="20"/>
<wire x1="-45" y1="40" x2="-40" y2="45" width="0" layer="20"/>
<wire x1="-40" y1="45" x2="40" y2="45" width="0" layer="20"/>
<wire x1="40" y1="45" x2="45" y2="40" width="0" layer="20"/>
<wire x1="45" y1="40" x2="45" y2="-40" width="0" layer="20"/>
<wire x1="45" y1="-40" x2="40" y2="-45" width="0" layer="20"/>
<pad name="P$1" x="-39.5" y="39.5" drill="2.9" diameter="7.5"/>
<pad name="P$2" x="-39.5" y="-39.5" drill="2.9" diameter="7.5"/>
<pad name="P$3" x="39.5" y="-39.5" drill="2.9" diameter="7.5"/>
<pad name="P$4" x="39.5" y="39.5" drill="2.9" diameter="7.5"/>
<wire x1="-33.02" y1="30.61" x2="-33.02" y2="40.77" width="0" layer="21"/>
<wire x1="-33.02" y1="40.77" x2="33.02" y2="40.77" width="0" layer="21"/>
<wire x1="33.02" y1="40.77" x2="33.02" y2="30.61" width="0" layer="21"/>
<wire x1="33.02" y1="30.61" x2="-33.02" y2="30.61" width="0" layer="21"/>
<text x="30" y="-36" size="2" layer="26" font="vector" rot="MR0">Title:
Date:
Board Template: V2.1</text>
<text x="20" y="-30" size="2" layer="26" font="vector" rot="MR0">&gt;DRAWING_NAME</text>
<text x="20" y="-33" size="2" layer="26" font="vector" rot="MR0">&gt;LAST_DATE_TIME</text>
<wire x1="-40" y1="-45" x2="-25" y2="-45" width="0" layer="20"/>
<wire x1="40" y1="-45" x2="19" y2="-45" width="0" layer="20"/>
</package>
<package name="BOARD_TEMPLATE_SILKONLY">
<wire x1="-40" y1="-45" x2="-45" y2="-40" width="0.2032" layer="21"/>
<wire x1="-45" y1="-40" x2="-45" y2="40" width="0.2032" layer="21"/>
<wire x1="-45" y1="40" x2="-40" y2="45" width="0.2032" layer="21"/>
<wire x1="-40" y1="45" x2="40" y2="45" width="0.2032" layer="21"/>
<wire x1="40" y1="45" x2="45" y2="40" width="0.2032" layer="21"/>
<wire x1="45" y1="40" x2="45" y2="-40" width="0.2032" layer="21"/>
<wire x1="45" y1="-40" x2="40" y2="-45" width="0.2032" layer="21"/>
<wire x1="40" y1="-45" x2="-40" y2="-45" width="0.2032" layer="21"/>
<pad name="P$1" x="-39.5" y="39.5" drill="2.9" diameter="7.5"/>
<pad name="P$2" x="-39.5" y="-39.5" drill="2.9" diameter="7.5"/>
<pad name="P$3" x="39.5" y="-39.5" drill="2.9" diameter="7.5"/>
<pad name="P$4" x="39.5" y="39.5" drill="2.9" diameter="7.5"/>
<wire x1="-33.02" y1="30.61" x2="-33.02" y2="40.77" width="0" layer="21"/>
<wire x1="-33.02" y1="40.77" x2="33.02" y2="40.77" width="0" layer="21"/>
<wire x1="33.02" y1="40.77" x2="33.02" y2="30.61" width="0" layer="21"/>
<wire x1="33.02" y1="30.61" x2="-33.02" y2="30.61" width="0" layer="21"/>
</package>
<package name="MT_AIRCORE_HORIZONTAL">
<wire x1="63" y1="0" x2="64" y2="1" width="0.001" layer="112" curve="90"/>
<wire x1="1" y1="7" x2="63" y2="7" width="0.001" layer="112"/>
<wire x1="64" y1="6" x2="64" y2="1" width="0.001" layer="112"/>
<wire x1="0" y1="1" x2="0" y2="6" width="0.001" layer="112"/>
<wire x1="64" y1="19" x2="63" y2="20" width="0.001" layer="112" curve="90"/>
<wire x1="0" y1="1" x2="1" y2="0" width="0.001" layer="112" curve="90"/>
<wire x1="1" y1="7" x2="0" y2="6" width="0.001" layer="112" curve="90"/>
<wire x1="1" y1="20" x2="0" y2="19" width="0.001" layer="112" curve="90"/>
<wire x1="64" y1="19" x2="64" y2="14" width="0.001" layer="112"/>
<wire x1="63" y1="0" x2="1" y2="0" width="0.001" layer="112"/>
<wire x1="0" y1="14" x2="0" y2="19" width="0.001" layer="112"/>
<wire x1="64" y1="6" x2="63" y2="7" width="0.001" layer="112" curve="90"/>
<wire x1="1" y1="20" x2="63" y2="20" width="0.001" layer="112"/>
<wire x1="63" y1="13" x2="1" y2="13" width="0.001" layer="112"/>
<wire x1="0" y1="14" x2="1" y2="13" width="0.001" layer="112" curve="90"/>
<wire x1="63" y1="13" x2="64" y2="14" width="0.001" layer="112" curve="90"/>
<wire x1="3.7" y1="10" x2="60.3" y2="10" width="0.001" layer="112"/>
<wire x1="3.7" y1="10.75" x2="60.3" y2="10.75" width="0.001" layer="112"/>
<wire x1="3.7" y1="11.5" x2="60.3" y2="11.5" width="0.001" layer="112"/>
<wire x1="3.7" y1="12.25" x2="60.3" y2="12.25" width="0.001" layer="112"/>
<wire x1="3.7" y1="9.25" x2="60.3" y2="9.25" width="0.001" layer="112"/>
<wire x1="3.7" y1="8.5" x2="60.3" y2="8.5" width="0.001" layer="112"/>
<wire x1="3.7" y1="7.75" x2="60.3" y2="7.75" width="0.001" layer="112"/>
<wire x1="3.7" y1="12.46" x2="3.7" y2="7.54" width="0.001" layer="112"/>
<wire x1="60.3" y1="12.46" x2="60.3" y2="7.54" width="0.001" layer="112"/>
<wire x1="32" y1="3.5" x2="27.96" y2="3.5" width="0.001" layer="112"/>
<wire x1="32" y1="3.5" x2="32" y2="-0.54" width="0.001" layer="112"/>
<wire x1="32" y1="3.5" x2="36.04" y2="3.5" width="0.001" layer="112"/>
<wire x1="3.7" y1="3.5" x2="-0.34" y2="3.5" width="0.001" layer="112"/>
<wire x1="3.7" y1="3.5" x2="3.7" y2="-0.54" width="0.001" layer="112"/>
<wire x1="3.7" y1="3.5" x2="7.74" y2="3.5" width="0.001" layer="112"/>
<wire x1="3.7" y1="16.5" x2="3.7" y2="20.54" width="0.001" layer="112"/>
<wire x1="3.7" y1="16.5" x2="-0.34" y2="16.5" width="0.001" layer="112"/>
<wire x1="3.7" y1="16.5" x2="7.74" y2="16.5" width="0.001" layer="112"/>
<wire x1="32" y1="16.5" x2="32" y2="20.54" width="0.001" layer="112"/>
<wire x1="32" y1="16.5" x2="27.96" y2="16.5" width="0.001" layer="112"/>
<wire x1="32" y1="16.5" x2="36.04" y2="16.5" width="0.001" layer="112"/>
<wire x1="60.3" y1="16.5" x2="60.3" y2="20.54" width="0.001" layer="112"/>
<wire x1="60.3" y1="16.5" x2="56.26" y2="16.5" width="0.001" layer="112"/>
<wire x1="60.3" y1="16.5" x2="64.34" y2="16.5" width="0.001" layer="112"/>
<wire x1="60.3" y1="3.5" x2="56.26" y2="3.5" width="0.001" layer="112"/>
<wire x1="60.3" y1="3.5" x2="60.3" y2="-0.54" width="0.001" layer="112"/>
<wire x1="60.3" y1="3.5" x2="64.34" y2="3.5" width="0.001" layer="112"/>
<hole x="3.7" y="16.5" drill="3.05"/>
<hole x="32" y="16.5" drill="3.05"/>
<hole x="60.3" y="16.5" drill="3.05"/>
<hole x="60.3" y="3.5" drill="3.05"/>
<hole x="32" y="3.5" drill="3.05"/>
<hole x="3.7" y="3.5" drill="3.05"/>
<pad name="A" x="30" y="22" drill="1.5" diameter="2.5" shape="square"/>
<pad name="B" x="34" y="22" drill="1.5" diameter="2.5" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="PC104_CONNECTOR">
<pin name="PPT_GND@1" x="-66.04" y="30.48" length="middle"/>
<pin name="PPT_VBAT@2" x="-12.7" y="30.48" length="middle" rot="R180"/>
<pin name="PPT_GND@3" x="12.7" y="30.48" length="middle"/>
<pin name="PPT_VBAT@4" x="63.5" y="30.48" length="middle" rot="R180"/>
<pin name="PPT_VBAT@5" x="-66.04" y="27.94" length="middle"/>
<pin name="PPT_GND@6" x="-12.7" y="27.94" length="middle" rot="R180"/>
<pin name="PPT_VBAT@7" x="12.7" y="27.94" length="middle"/>
<pin name="PPT_GND@8" x="63.5" y="27.94" length="middle" rot="R180"/>
<pin name="PPT_GND@9" x="-66.04" y="25.4" length="middle"/>
<pin name="PPT_VBAT@" x="-12.7" y="25.4" length="middle" rot="R180"/>
<pin name="PPT_GND@11" x="12.7" y="25.4" length="middle"/>
<pin name="PPT_VBAT@12" x="63.5" y="25.4" length="middle" rot="R180"/>
<pin name="PPT_VBAT@13" x="-66.04" y="22.86" length="middle"/>
<pin name="PPT_GND@14" x="-12.7" y="22.86" length="middle" rot="R180"/>
<pin name="PPT_VBAT@15" x="12.7" y="22.86" length="middle"/>
<pin name="PPT_GND@16" x="63.5" y="22.86" length="middle" rot="R180"/>
<pin name="DISTRI_GND@17" x="-66.04" y="20.32" length="middle"/>
<pin name="DISTRI_VBAT@18" x="-12.7" y="20.32" length="middle" rot="R180"/>
<pin name="DISTRI_GND@19" x="12.7" y="20.32" length="middle"/>
<pin name="DISTRI_VBAT@20" x="63.5" y="20.32" length="middle" rot="R180"/>
<pin name="DISTRI_VBAT@21" x="-66.04" y="17.78" length="middle"/>
<pin name="DISTRI_GND@22" x="-12.7" y="17.78" length="middle" rot="R180"/>
<pin name="DISTRI_VBAT@23" x="12.7" y="17.78" length="middle"/>
<pin name="DISTRI_GND@24" x="63.5" y="17.78" length="middle" rot="R180"/>
<pin name="DISTRI_GND@25" x="-66.04" y="15.24" length="middle"/>
<pin name="DISTRI_VBAT@26" x="-12.7" y="15.24" length="middle" rot="R180"/>
<pin name="DISTRI_GND@27" x="12.7" y="15.24" length="middle"/>
<pin name="DISTRI_VBAT@28" x="63.5" y="15.24" length="middle" rot="R180"/>
<pin name="DISTRI_VBAT@29" x="-66.04" y="12.7" length="middle"/>
<pin name="DISTRI_GND@30" x="-12.7" y="12.7" length="middle" rot="R180"/>
<pin name="DISTRI_VBAT@31" x="12.7" y="12.7" length="middle"/>
<pin name="DISTRI_GND@32" x="63.5" y="12.7" length="middle" rot="R180"/>
<pin name="BATT_VBAT$33" x="-66.04" y="10.16" length="middle"/>
<pin name="BATT_VBATT$34" x="-12.7" y="10.16" length="middle" rot="R180"/>
<pin name="GEN_VBATT$35" x="12.7" y="10.16" length="middle"/>
<pin name="GEN_VBATT$36" x="63.5" y="10.16" length="middle" rot="R180"/>
<pin name="MT_X_A@1" x="-66.04" y="7.62" length="middle"/>
<pin name="MT_X_A@2" x="-12.7" y="7.62" length="middle" rot="R180"/>
<pin name="MT_Y_A@1" x="12.7" y="7.62" length="middle"/>
<pin name="MT_Y_A@2" x="63.5" y="7.62" length="middle" rot="R180"/>
<pin name="MT_X_B@1" x="-66.04" y="5.08" length="middle"/>
<pin name="MT_X_B@2" x="-12.7" y="5.08" length="middle" rot="R180"/>
<pin name="MT_Y_B@1" x="12.7" y="5.08" length="middle"/>
<pin name="MT_Y_B@2" x="63.5" y="5.08" length="middle" rot="R180"/>
<pin name="SYNC1" x="-66.04" y="2.54" length="middle"/>
<pin name="P$46" x="-12.7" y="2.54" length="middle" rot="R180"/>
<pin name="P$47" x="12.7" y="2.54" length="middle"/>
<pin name="P$48" x="63.5" y="2.54" length="middle" rot="R180"/>
<pin name="SYNC2" x="-66.04" y="0" length="middle"/>
<pin name="P$50" x="-12.7" y="0" length="middle" rot="R180"/>
<pin name="TOPDEPLOY+@51" x="12.7" y="0" length="middle"/>
<pin name="TOPDEPLOY-$52" x="63.5" y="0" length="middle" rot="R180"/>
<pin name="CANH" x="-66.04" y="-2.54" length="middle"/>
<pin name="P$54" x="-12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="TOPDEPLOY+@55" x="12.7" y="-2.54" length="middle"/>
<pin name="TOPDEPLOY-$56" x="63.5" y="-2.54" length="middle" rot="R180"/>
<pin name="CANL" x="-66.04" y="-5.08" length="middle"/>
<pin name="P$58" x="-12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="P@59" x="12.7" y="-5.08" length="middle"/>
<pin name="P$60" x="63.5" y="-5.08" length="middle" rot="R180"/>
<pin name="P$61" x="-66.04" y="-7.62" length="middle"/>
<pin name="P$62" x="-12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="P$63" x="12.7" y="-7.62" length="middle"/>
<pin name="P$64" x="63.5" y="-7.62" length="middle" rot="R180"/>
<pin name="P$65" x="-66.04" y="-10.16" length="middle"/>
<pin name="P$66" x="-12.7" y="-10.16" length="middle" rot="R180"/>
<pin name="P$67" x="12.7" y="-10.16" length="middle"/>
<pin name="P$68" x="63.5" y="-10.16" length="middle" rot="R180"/>
<pin name="WHEEL_VBAT@69" x="-66.04" y="-12.7" length="middle"/>
<pin name="WHEEL_VBAT@70" x="-12.7" y="-12.7" length="middle" rot="R180"/>
<pin name="WHEEL_GND@71" x="12.7" y="-12.7" length="middle"/>
<pin name="WHEEL_GND@72" x="63.5" y="-12.7" length="middle" rot="R180"/>
<pin name="P$73" x="-66.04" y="-15.24" length="middle"/>
<pin name="P$74" x="-12.7" y="-15.24" length="middle" rot="R180"/>
<pin name="P$75" x="12.7" y="-15.24" length="middle"/>
<pin name="P$76" x="63.5" y="-15.24" length="middle" rot="R180"/>
<pin name="ESTIMAT_VBAT@77" x="-66.04" y="-17.78" length="middle"/>
<pin name="ESTIMAT_VBAT@78" x="-12.7" y="-17.78" length="middle" rot="R180"/>
<pin name="ESTIMAT_GND@79" x="12.7" y="-17.78" length="middle"/>
<pin name="ESTIMAT_GND@80" x="63.5" y="-17.78" length="middle" rot="R180"/>
<pin name="P$81" x="-66.04" y="-20.32" length="middle"/>
<pin name="P$82" x="-12.7" y="-20.32" length="middle" rot="R180"/>
<pin name="P$83" x="12.7" y="-20.32" length="middle"/>
<pin name="P$84" x="63.5" y="-20.32" length="middle" rot="R180"/>
<pin name="BDOT_VBAT@85" x="-66.04" y="-22.86" length="middle"/>
<pin name="BDOT_VBAT@86" x="-12.7" y="-22.86" length="middle" rot="R180"/>
<pin name="BDOT_GND@87" x="12.7" y="-22.86" length="middle"/>
<pin name="BDOT_GND@88" x="63.5" y="-22.86" length="middle" rot="R180"/>
<pin name="P$89" x="-66.04" y="-25.4" length="middle"/>
<pin name="P$90" x="-12.7" y="-25.4" length="middle" rot="R180"/>
<pin name="P$91" x="12.7" y="-25.4" length="middle"/>
<pin name="P$92" x="63.5" y="-25.4" length="middle" rot="R180"/>
<pin name="RAHS_VBAT@93" x="-66.04" y="-27.94" length="middle"/>
<pin name="RAHS_GND@94" x="-12.7" y="-27.94" length="middle" rot="R180"/>
<pin name="P$95" x="12.7" y="-27.94" length="middle"/>
<pin name="P$96" x="63.5" y="-27.94" length="middle" rot="R180"/>
<pin name="COM2_VBAT@97" x="-66.04" y="-30.48" length="middle"/>
<pin name="COM2_GND@98" x="-12.7" y="-30.48" length="middle" rot="R180"/>
<pin name="COM2_VBAT@99" x="12.7" y="-30.48" length="middle"/>
<pin name="COM2_GND@100" x="63.5" y="-30.48" length="middle" rot="R180"/>
<pin name="COM1_VBAT@101" x="-66.04" y="-33.02" length="middle"/>
<pin name="COM1_GND@102" x="-12.7" y="-33.02" length="middle" rot="R180"/>
<pin name="COM1_VBAT@103" x="12.7" y="-33.02" length="middle"/>
<pin name="COM1_GND@104" x="63.5" y="-33.02" length="middle" rot="R180"/>
<wire x1="-60.96" y1="-35.56" x2="-60.96" y2="33.02" width="0.254" layer="94"/>
<wire x1="-60.96" y1="33.02" x2="-17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="33.02" x2="-17.78" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-35.56" x2="-60.96" y2="-35.56" width="0.254" layer="94"/>
<wire x1="17.78" y1="-35.56" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="58.42" y2="33.02" width="0.254" layer="94"/>
<wire x1="58.42" y1="33.02" x2="58.42" y2="-35.56" width="0.254" layer="94"/>
<wire x1="58.42" y1="-35.56" x2="17.78" y2="-35.56" width="0.254" layer="94"/>
<text x="-48.768" y="-38.862" size="1.778" layer="94">version: V2.4</text>
<wire x1="-59.69" y1="-6.35" x2="57.15" y2="-6.35" width="0" layer="98"/>
<wire x1="57.15" y1="-6.35" x2="57.15" y2="-29.21" width="0" layer="98"/>
<wire x1="57.15" y1="-29.21" x2="7.62" y2="-29.21" width="0" layer="98"/>
<wire x1="7.62" y1="-29.21" x2="7.62" y2="-26.67" width="0" layer="98"/>
<wire x1="7.62" y1="-26.67" x2="-59.69" y2="-26.67" width="0" layer="98"/>
<wire x1="-59.69" y1="-26.67" x2="-59.69" y2="-6.35" width="0" layer="98"/>
<text x="29.21" y="-10.16" size="1.778" layer="98">ADCS reserved</text>
<text x="-46.99" y="-10.16" size="1.778" layer="98">ADCS reserved</text>
</symbol>
<symbol name="BOARD_TEMPLATE">
<text x="0" y="0" size="1.778" layer="94">Board Template: V2.1</text>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="25.4" y2="3.81" width="0.254" layer="94"/>
<wire x1="25.4" y1="3.81" x2="25.4" y2="-1.27" width="0.254" layer="94"/>
<wire x1="25.4" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="MT_AIRCORE_HORIZONTAL">
<text x="0" y="0" size="2.54" layer="94" font="vector" align="center">MT_AIRCORE
HORIZONTAL</text>
<wire x1="-17.78" y1="7.62" x2="17.78" y2="7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="7.62" x2="17.78" y2="-7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="-7.62" x2="-17.78" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-7.62" x2="-17.78" y2="7.62" width="0.254" layer="94"/>
<pin name="A" x="-22.86" y="0" visible="pin" length="middle"/>
<pin name="B" x="22.86" y="0" visible="pin" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PC104_CONNECTOR">
<gates>
<gate name="G$1" symbol="PC104_CONNECTOR" x="-2.54" y="30.48"/>
</gates>
<devices>
<device name="" package="PC104_CONNECTOR">
<connects>
<connect gate="G$1" pin="BATT_VBAT$33" pad="P$33"/>
<connect gate="G$1" pin="BATT_VBATT$34" pad="P$34"/>
<connect gate="G$1" pin="BDOT_GND@87" pad="P$87"/>
<connect gate="G$1" pin="BDOT_GND@88" pad="P$88"/>
<connect gate="G$1" pin="BDOT_VBAT@85" pad="P$85"/>
<connect gate="G$1" pin="BDOT_VBAT@86" pad="P$86"/>
<connect gate="G$1" pin="CANH" pad="P$53"/>
<connect gate="G$1" pin="CANL" pad="P$57"/>
<connect gate="G$1" pin="COM1_GND@102" pad="P$102"/>
<connect gate="G$1" pin="COM1_GND@104" pad="P$104"/>
<connect gate="G$1" pin="COM1_VBAT@101" pad="P$101"/>
<connect gate="G$1" pin="COM1_VBAT@103" pad="P$103"/>
<connect gate="G$1" pin="COM2_GND@100" pad="P$100"/>
<connect gate="G$1" pin="COM2_GND@98" pad="P$98"/>
<connect gate="G$1" pin="COM2_VBAT@97" pad="P$97"/>
<connect gate="G$1" pin="COM2_VBAT@99" pad="P$99"/>
<connect gate="G$1" pin="DISTRI_GND@17" pad="P$17"/>
<connect gate="G$1" pin="DISTRI_GND@19" pad="P$19"/>
<connect gate="G$1" pin="DISTRI_GND@22" pad="P$22"/>
<connect gate="G$1" pin="DISTRI_GND@24" pad="P$24"/>
<connect gate="G$1" pin="DISTRI_GND@25" pad="P$25"/>
<connect gate="G$1" pin="DISTRI_GND@27" pad="P$27"/>
<connect gate="G$1" pin="DISTRI_GND@30" pad="P$30"/>
<connect gate="G$1" pin="DISTRI_GND@32" pad="P$32"/>
<connect gate="G$1" pin="DISTRI_VBAT@18" pad="P$18"/>
<connect gate="G$1" pin="DISTRI_VBAT@20" pad="P$20"/>
<connect gate="G$1" pin="DISTRI_VBAT@21" pad="P$21"/>
<connect gate="G$1" pin="DISTRI_VBAT@23" pad="P$23"/>
<connect gate="G$1" pin="DISTRI_VBAT@26" pad="P$26"/>
<connect gate="G$1" pin="DISTRI_VBAT@28" pad="P$28"/>
<connect gate="G$1" pin="DISTRI_VBAT@29" pad="P$29"/>
<connect gate="G$1" pin="DISTRI_VBAT@31" pad="P$31"/>
<connect gate="G$1" pin="ESTIMAT_GND@79" pad="P$79"/>
<connect gate="G$1" pin="ESTIMAT_GND@80" pad="P$80"/>
<connect gate="G$1" pin="ESTIMAT_VBAT@77" pad="P$77"/>
<connect gate="G$1" pin="ESTIMAT_VBAT@78" pad="P$78"/>
<connect gate="G$1" pin="GEN_VBATT$35" pad="P$35"/>
<connect gate="G$1" pin="GEN_VBATT$36" pad="P$36"/>
<connect gate="G$1" pin="MT_X_A@1" pad="P$37"/>
<connect gate="G$1" pin="MT_X_A@2" pad="P$38"/>
<connect gate="G$1" pin="MT_X_B@1" pad="P$41"/>
<connect gate="G$1" pin="MT_X_B@2" pad="P$42"/>
<connect gate="G$1" pin="MT_Y_A@1" pad="P$39"/>
<connect gate="G$1" pin="MT_Y_A@2" pad="P$40"/>
<connect gate="G$1" pin="MT_Y_B@1" pad="P$43"/>
<connect gate="G$1" pin="MT_Y_B@2" pad="P$44"/>
<connect gate="G$1" pin="P$46" pad="P$46"/>
<connect gate="G$1" pin="P$47" pad="P$47"/>
<connect gate="G$1" pin="P$48" pad="P$48"/>
<connect gate="G$1" pin="P$50" pad="P$50"/>
<connect gate="G$1" pin="P$54" pad="P$54"/>
<connect gate="G$1" pin="P$58" pad="P$58"/>
<connect gate="G$1" pin="P$60" pad="P$60"/>
<connect gate="G$1" pin="P$61" pad="P$61"/>
<connect gate="G$1" pin="P$62" pad="P$62"/>
<connect gate="G$1" pin="P$63" pad="P$63"/>
<connect gate="G$1" pin="P$64" pad="P$64"/>
<connect gate="G$1" pin="P$65" pad="P$65"/>
<connect gate="G$1" pin="P$66" pad="P$66"/>
<connect gate="G$1" pin="P$67" pad="P$67"/>
<connect gate="G$1" pin="P$68" pad="P$68"/>
<connect gate="G$1" pin="P$73" pad="P$73"/>
<connect gate="G$1" pin="P$74" pad="P$74"/>
<connect gate="G$1" pin="P$75" pad="P$75"/>
<connect gate="G$1" pin="P$76" pad="P$76"/>
<connect gate="G$1" pin="P$81" pad="P$81"/>
<connect gate="G$1" pin="P$82" pad="P$82"/>
<connect gate="G$1" pin="P$83" pad="P$83"/>
<connect gate="G$1" pin="P$84" pad="P$84"/>
<connect gate="G$1" pin="P$89" pad="P$89"/>
<connect gate="G$1" pin="P$90" pad="P$90"/>
<connect gate="G$1" pin="P$91" pad="P$91"/>
<connect gate="G$1" pin="P$92" pad="P$92"/>
<connect gate="G$1" pin="P$95" pad="P$95"/>
<connect gate="G$1" pin="P$96" pad="P$96"/>
<connect gate="G$1" pin="P@59" pad="P$59"/>
<connect gate="G$1" pin="PPT_GND@1" pad="P$1"/>
<connect gate="G$1" pin="PPT_GND@11" pad="P$11"/>
<connect gate="G$1" pin="PPT_GND@14" pad="P$14"/>
<connect gate="G$1" pin="PPT_GND@16" pad="P$16"/>
<connect gate="G$1" pin="PPT_GND@3" pad="P$3"/>
<connect gate="G$1" pin="PPT_GND@6" pad="P$6"/>
<connect gate="G$1" pin="PPT_GND@8" pad="P$8"/>
<connect gate="G$1" pin="PPT_GND@9" pad="P$9"/>
<connect gate="G$1" pin="PPT_VBAT@" pad="P$10"/>
<connect gate="G$1" pin="PPT_VBAT@12" pad="P$12"/>
<connect gate="G$1" pin="PPT_VBAT@13" pad="P$13"/>
<connect gate="G$1" pin="PPT_VBAT@15" pad="P$15"/>
<connect gate="G$1" pin="PPT_VBAT@2" pad="P$2"/>
<connect gate="G$1" pin="PPT_VBAT@4" pad="P$4"/>
<connect gate="G$1" pin="PPT_VBAT@5" pad="P$5"/>
<connect gate="G$1" pin="PPT_VBAT@7" pad="P$7"/>
<connect gate="G$1" pin="RAHS_GND@94" pad="P$94"/>
<connect gate="G$1" pin="RAHS_VBAT@93" pad="P$93"/>
<connect gate="G$1" pin="SYNC1" pad="P$45"/>
<connect gate="G$1" pin="SYNC2" pad="P$49"/>
<connect gate="G$1" pin="TOPDEPLOY+@51" pad="P$51"/>
<connect gate="G$1" pin="TOPDEPLOY+@55" pad="P$55"/>
<connect gate="G$1" pin="TOPDEPLOY-$52" pad="P$52"/>
<connect gate="G$1" pin="TOPDEPLOY-$56" pad="P$56"/>
<connect gate="G$1" pin="WHEEL_GND@71" pad="P$71"/>
<connect gate="G$1" pin="WHEEL_GND@72" pad="P$72"/>
<connect gate="G$1" pin="WHEEL_VBAT@69" pad="P$69"/>
<connect gate="G$1" pin="WHEEL_VBAT@70" pad="P$70"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BOARD_TEMPLATE">
<gates>
<gate name="G$1" symbol="BOARD_TEMPLATE" x="0" y="0"/>
</gates>
<devices>
<device name="1" package="BOARD_TEMPLATE">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SERCURED" package="BOARD_TEMPLATE_SECURED">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RAHS" package="BOARD_TEMPLATE_RAHS">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SILKONLY" package="BOARD_TEMPLATE_SILKONLY">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3D" package="BOARD_TEMPLATE">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MT_AIRCORE_HORIZONTAL">
<gates>
<gate name="G$1" symbol="MT_AIRCORE_HORIZONTAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MT_AIRCORE_HORIZONTAL">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="2X4">
<description>&lt;h3&gt;Plated Through Hole - 2x4&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:8&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.2032" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.2032" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.2032" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.2032" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" diameter="1.8796"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<wire x1="-4.445" y1="-2.794" x2="-3.175" y2="-2.794" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-2.794" x2="-4.445" y2="-2.794" width="0.2032" layer="22"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.2032" layer="21"/>
<text x="-5.08" y="2.794" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-5.08" y="-3.683" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_04X2">
<description>&lt;h3&gt;8 Pin Connection&lt;/h3&gt;
4x2 pin layout</description>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<text x="-3.81" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-4.064" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_04X2" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_04X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="DubSat1 Components" deviceset="PC104_CONNECTOR" device=""/>
<part name="U$2" library="DubSat1 Components" deviceset="BOARD_TEMPLATE" device="SERCURED"/>
<part name="MT_X" library="DubSat1 Components" deviceset="MT_AIRCORE_HORIZONTAL" device=""/>
<part name="MT_Y" library="DubSat1 Components" deviceset="MT_AIRCORE_HORIZONTAL" device=""/>
<part name="J1" library="SparkFun-Connectors" deviceset="CONN_04X2" device=""/>
<part name="J2" library="SparkFun-Connectors" deviceset="CONN_04X2" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="60.96" y="38.1"/>
<instance part="U$2" gate="G$1" x="0" y="83.82"/>
<instance part="MT_X" gate="G$1" x="20.32" y="-43.18"/>
<instance part="MT_Y" gate="G$1" x="99.06" y="-43.18"/>
<instance part="J1" gate="G$1" x="20.32" y="-17.78" rot="R90"/>
<instance part="J2" gate="G$1" x="99.06" y="-17.78" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="MT_X_A" class="0">
<segment>
<pinref part="MT_X" gate="G$1" pin="A"/>
<wire x1="-2.54" y1="-43.18" x2="-5.08" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-43.18" x2="-5.08" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-30.48" x2="15.24" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="15.24" y1="-30.48" x2="15.24" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="15.24" y1="-30.48" x2="17.78" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-30.48" x2="17.78" y2="-25.4" width="0.1524" layer="91"/>
<junction x="15.24" y="-30.48"/>
</segment>
</net>
<net name="MT_X_B" class="0">
<segment>
<pinref part="MT_X" gate="G$1" pin="B"/>
<wire x1="43.18" y1="-43.18" x2="45.72" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-43.18" x2="45.72" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-30.48" x2="22.86" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="7"/>
<wire x1="22.86" y1="-30.48" x2="22.86" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="5"/>
<wire x1="20.32" y1="-25.4" x2="20.32" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-30.48" x2="22.86" y2="-30.48" width="0.1524" layer="91"/>
<junction x="22.86" y="-30.48"/>
</segment>
</net>
<net name="MT_Y_A" class="0">
<segment>
<pinref part="MT_Y" gate="G$1" pin="A"/>
<wire x1="76.2" y1="-43.18" x2="73.66" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-43.18" x2="73.66" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="73.66" y1="-30.48" x2="93.98" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-30.48" x2="93.98" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="93.98" y1="-30.48" x2="96.52" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-30.48" x2="96.52" y2="-25.4" width="0.1524" layer="91"/>
<junction x="93.98" y="-30.48"/>
</segment>
</net>
<net name="MT_Y_B" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="5"/>
<wire x1="99.06" y1="-25.4" x2="99.06" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-30.48" x2="101.6" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-30.48" x2="124.46" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-30.48" x2="124.46" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="MT_Y" gate="G$1" pin="B"/>
<wire x1="124.46" y1="-43.18" x2="121.92" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="7"/>
<wire x1="101.6" y1="-25.4" x2="101.6" y2="-30.48" width="0.1524" layer="91"/>
<junction x="101.6" y="-30.48"/>
</segment>
</net>
<net name="MT_X_A_STACK" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="17.78" y1="-10.16" x2="17.78" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-5.08" x2="15.24" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="15.24" y1="-5.08" x2="15.24" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-5.08" x2="-15.24" y2="-5.08" width="0.1524" layer="91"/>
<junction x="15.24" y="-5.08"/>
<wire x1="-15.24" y1="-5.08" x2="-15.24" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="MT_X_A@1"/>
<wire x1="-15.24" y1="45.72" x2="-5.08" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="45.72" x2="-15.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="76.2" x2="55.88" y2="76.2" width="0.1524" layer="91"/>
<junction x="-15.24" y="45.72"/>
<wire x1="55.88" y1="76.2" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="MT_X_A@2"/>
<wire x1="55.88" y1="45.72" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MT_X_B_STACK" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="6"/>
<wire x1="20.32" y1="-10.16" x2="20.32" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-2.54" x2="22.86" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="8"/>
<wire x1="22.86" y1="-2.54" x2="22.86" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-2.54" x2="55.88" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-2.54" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<junction x="22.86" y="-2.54"/>
<pinref part="U$1" gate="G$1" pin="MT_X_B@2"/>
<wire x1="55.88" y1="43.18" x2="48.26" y2="43.18" width="0.1524" layer="91"/>
<junction x="20.32" y="-2.54"/>
<wire x1="20.32" y1="-2.54" x2="-12.7" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="MT_X_B@1"/>
<wire x1="-12.7" y1="-2.54" x2="-12.7" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="43.18" x2="-5.08" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MT_Y_A_STACK" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="93.98" y1="-10.16" x2="93.98" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-5.08" x2="96.52" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="96.52" y1="-5.08" x2="96.52" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-5.08" x2="66.04" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-5.08" x2="66.04" y2="45.72" width="0.1524" layer="91"/>
<junction x="93.98" y="-5.08"/>
<pinref part="U$1" gate="G$1" pin="MT_Y_A@1"/>
<wire x1="66.04" y1="45.72" x2="73.66" y2="45.72" width="0.1524" layer="91"/>
<wire x1="66.04" y1="45.72" x2="66.04" y2="76.2" width="0.1524" layer="91"/>
<junction x="66.04" y="45.72"/>
<wire x1="66.04" y1="76.2" x2="129.54" y2="76.2" width="0.1524" layer="91"/>
<wire x1="129.54" y1="76.2" x2="129.54" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="MT_Y_A@2"/>
<wire x1="129.54" y1="45.72" x2="124.46" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MT_Y_B_STACK" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="6"/>
<wire x1="99.06" y1="-10.16" x2="99.06" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-2.54" x2="101.6" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="8"/>
<wire x1="101.6" y1="-2.54" x2="101.6" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-2.54" x2="129.54" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-2.54" x2="129.54" y2="43.18" width="0.1524" layer="91"/>
<junction x="101.6" y="-2.54"/>
<pinref part="U$1" gate="G$1" pin="MT_Y_B@2"/>
<wire x1="129.54" y1="43.18" x2="124.46" y2="43.18" width="0.1524" layer="91"/>
<junction x="99.06" y="-2.54"/>
<wire x1="99.06" y1="-2.54" x2="68.58" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-2.54" x2="68.58" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="MT_Y_B@1"/>
<wire x1="68.58" y1="43.18" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
