/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: MSP_SP0_data.c
 *
 * Code generated for Simulink model 'MSP_SP0'.
 *
 * Model version                  : 1.345
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Sat Apr 14 22:03:33 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Texas Instruments->MSP430
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "MSP_SP0.h"

/* Constant parameters (auto storage) */
const ConstP rtConstP = {
  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<S2>/Constant1'
   *   '<S3>/process_matrix'
   *   '<S3>/sensor2body'
   *   '<S4>/process_matrix'
   *   '<S4>/sensor2body'
   */
  { 1.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 1.0F }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
